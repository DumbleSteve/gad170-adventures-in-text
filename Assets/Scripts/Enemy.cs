
using System.Collections.Generic;
using UnityEngine;

/*
 * All data and functions relating to an Enemy.
 * An Enemy takes damage from Player attacks,
 * depleting its health until it is defeated.
 */
public class Enemy : MonoBehaviour
{
    //public string enemyName = "Enemy";      // can override in inspector
    public string EnemyName { get; private set; }        // set randomly by Combat at start of game

    // level data
    // enemy is assigned a random level when 'spawned' (reset)
    private int level = 1;
    private readonly int minLevel = 1;
    private readonly int maxLevel = 10;

    // HP data
    private readonly int baseHealthPoints = 15; // for level 1
    private int healthPoints;                   // reduced when enemy takes damage

    // effectively 'spawns' a new enemy, by resetting the single enemy in the game
    public void ResetStats(string enemyType)
    {
        EnemyName = enemyType;

        // set random level and therefore HP
        level = Random.Range(minLevel, maxLevel);
        healthPoints = baseHealthPoints * level;        // multiply

        Debug.Log(">\n");
        Debug.Log($"<color=orange>... An enemy {EnemyName} has suddenly appeared! ...</color>");
        Debug.Log($"<color=orange>    Level: {level}  HP: {healthPoints} ...</color>");
    }

    // Damage is inflicted on the enemy by reducing HP.
    // Returns false if enemy survived the attack.
    // Returns true if enemy was defeated.
    public bool TakeDamage(int attackDamage)
    {
        // reduce HP
        healthPoints -= attackDamage;

        if (healthPoints < 0)
            healthPoints = 0;

        Debug.Log($"<color=orange>The {EnemyName} sustained {attackDamage} damage!  HP is now {healthPoints}</color>");

        if (healthPoints <= 0)
            return true;             // enemy defeated!

        return false;                // still alive!
    }
}
