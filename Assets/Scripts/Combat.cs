
using System.Collections.Generic;       // needed for Lists, etc.
using UnityEngine;

/*
 * Manages combat between a Player and an Enemy.
 * Detects player keyboard input to attack enemy and Level-up (when possible).
 * When the Enemy is defeated, it is 'respawned' (reset) with a random Level and HP.
 * Game over when Player reaches Level 5 -> game starts again.
 */
public class Combat : MonoBehaviour
{
    public Player combatPlayer;                 // set in inspector
    public Enemy combatEnemy;                   // set in inspector

    public List<string> enemyNames;             // set in inspector (monster, demon, orc, etc)

    public KeyCode attackKey = KeyCode.A;       // can override in inspector
    public KeyCode levelUpKey = KeyCode.L;      // can override in inspector

    public int gameOverLevel = 5;               // 'game over' when player reaches this level


    // Return a random enemy name (string) from the list.
    // Example of using an 'index' (0 is the first item) to get a specific value from a list
    // (very) expanded version...
    private string RandomEnemyName
    {
        get
        {
            if (enemyNames.Count == 0)
                return "Enemy";                 // go no further if no enemy names!

            int numberOfEnemyNames = enemyNames.Count;
            int randomEnemyIndex = Random.Range(0, numberOfEnemyNames);
            string randomType = enemyNames[ randomEnemyIndex ];               // [] denotes the (zero-based) index of an item in the list
            return randomType;
        }
    }

    // condensed version =)
    //private string RandomEnemyName => (enemyNames.Count > 0) ? enemyNames[Random.Range(0, enemyNames.Count)] : "Enemy";

    // Start is called before the first frame Update.
    // Immediately start combat, which resets the the player and enemy
    private void Start()
    {
        StartCombat();
    }

    // Reset player and enemy to start a new battle
    // at start of game and on game over.
    private void StartCombat()
    {
        Debug.Log(">\n");
        Debug.Log($"<color=yellow>YOU FIND YOURSELF IN A DARK, FORBODING PLACE ...or something... </color>");

        combatPlayer.ResetStats();                       // level 1, zero EXP
        combatEnemy.ResetStats(RandomEnemyName);         // effectively 'spawns' a new enemy, with random name, level & HP

        Debug.Log($"<color=yellow> --->> {combatPlayer.playerName} vs. {combatEnemy.EnemyName} <<--- </color>");
        Debug.Log($"<color=lime>    Press '{attackKey}' to Attack the {combatEnemy.EnemyName}!</color>");
        Debug.Log(">\n");
    }

    // Update is called once per frame
    // Check every frame to see if player has pressed attack or level-up keys
    private void Update()
    {
        if (Input.GetKeyDown(attackKey))
        {
            if (combatPlayer.Attack(combatEnemy))       // Attack function returns true if enemy was defeated by the attack
            {
                // enemy defeated!

                // player has gained EXP by defeating enemy - may be able to level-up!
                if (combatPlayer.canLevelUp)
                {
                    Debug.Log(">\n");
                    Debug.Log($"<color=lime>    Press '{levelUpKey}' to Level-Up!</color>");
                }

                // 'spawn' a new enemy (by resetting Enemy data with random name, level and HP)
                combatEnemy.ResetStats(RandomEnemyName);
            }
        }
        else if (Input.GetKeyDown(levelUpKey))
        {
            if (combatPlayer.canLevelUp)        // player has enough EXP to level up
            {
                combatPlayer.LevelUp();

                // game over?
                if (combatPlayer.currentLevel == gameOverLevel)     // == means 'is equal to'
                {
                    GameOver();
                }
            }
            else        // not enough EXP yet to level up!
            {
                Debug.Log($"<color=red>{combatPlayer.playerName} needs more EXP to Level-up!</color>");
            }
        }
    }

    // restart game
    private void GameOver()
    {
        Debug.Log(">\n");
        Debug.Log($"<color=magenta>--->> VICTORY!! The {combatPlayer.playerName} has vanquished the {combatEnemy.EnemyName}!! <<---</color>");

        StartCombat();      // play again!  reset player and enemy
    }
}
