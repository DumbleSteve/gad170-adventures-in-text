
using UnityEngine;

/*
 * All data/stats/variables and functions relating to a Player.
 * The Player can inflict damage on Enemies to gain EXP and Level-up.
 */
public class Player : MonoBehaviour
{
    // player data
    public string playerName = "Player";                // can override in inspector
    public int currentLevel = 1;
    public bool canLevelUp = false;                     // set to true at the point that currentExp hits expLevelUpThreshold

    // EXP data
    public int currentEXP = 0;
    private readonly int minExp = 1;                    // for random EXP upgrade when an enemy is defeated
    private readonly int maxExp = 50;                   // for random EXP upgrade when an enemy is defeated

    // level up data
    private int expLevelUpThreshold;                    // EXP required to level up to next level
    private readonly int nextLevelIncrease = 10;         // multiplied by level to set levelUpThreshold on level up

    //// Returns the amount of EXP required to level-up
    //// Example of a 'getter', which can be seen as a variable
    //// that is calculated 'on the fly' when needed, via 'return' statement (like a function)
    //// Can also be written as:
    //// public int LevelUpExpRequired => expLevelUpThreshold - CurrentEXP;
    //public int LevelUpExpRequired
    //{
    //    get { return expLevelUpThreshold - currentEXP; }
    //}

    // attack data
    private readonly int baseAttackDamage = 10;         // must be int according to brief...
    private readonly float levelUpAttackMultiplier = 1.25f;      // as per brief - attack damage increases by this much on each level up
    private int currentAttackDamage = 0;                // recalculated on each level-up


    // reset Level and EXP at start of game and after game over
    public void ResetStats()
    {
        currentLevel = 1;
        canLevelUp = false;
        currentAttackDamage = baseAttackDamage;
        currentEXP = 0;

        Debug.Log($"<color=cyan>... WELCOME Brave {playerName}! ...</color>");
        Debug.Log($"<color=cyan>    Level: {currentLevel}  EXP: {currentEXP}  Attack damage: {currentAttackDamage}</color>");

        SetExpLevelUpThreshold();       // set EXP required to level up
    }

    // Inflict damage on the enemy.
    // Returns false if enemy survived attack.
    // Returns true if enemy defeated
    public bool Attack(Enemy enemy)
    {
        Debug.Log($"<color=cyan>The {playerName} attacked the {enemy.EnemyName}!!</color>");

        // inflict damage on the enemy
        bool enemyDefeated = enemy.TakeDamage(currentAttackDamage);

        // if the enemy was defeated, increase Player's EXP by a random amount
        if (enemyDefeated)
        {
            currentEXP += Random.Range(minExp, maxExp);     // same as currentEXP = currentEXP + Random.Range(minExp, maxExp)

            // Player can level up once they have reached the level-up threshold
            if (currentEXP >= expLevelUpThreshold)      // >= means 'greater than or equal to'
            {
                canLevelUp = true;          // user must press key to level up
            }
            Debug.Log(">\n");
            Debug.Log($"<color=cyan>{enemy.EnemyName} defeated! {playerName} EXP upgraded to {currentEXP}</color>");
        }

        return enemyDefeated;
    }

    // If player has sufficient EXP, increment currentLevel, increase attack damage
    // and set the EXP threshold for the next level-up
    public int LevelUp()
    {
        if (canLevelUp)
        {
            currentLevel++;                 // add 1 to level - same as currentLevel = currentLevel + 1;
            Debug.Log(">\n");
            Debug.Log($"<color=cyan>The {playerName} is now at Level {currentLevel}!!</color>");

            LevelUpAttackDamage();          // attack damage increases on every level-up
            SetExpLevelUpThreshold();       // set EXP threshold for next level up

            canLevelUp = false;             // reset until enough EXP gained for next level
        }

        return currentLevel;
    }

    // Attack damage increases by a factor of 1.25 (as per brief) on every level-up
    // Because we are multiplying an int (currentAttackDamage) and a float (levelUpAttackMultiplier),
    // we typecast the int to a float to preserve precision (ie. prevent everything being cast to an int and losing precision).
    // We then typecast the result back to an int for the next enemy TakeDamage function call when the player attacks
    private void LevelUpAttackDamage()
    {
        currentAttackDamage = (int)((float)currentAttackDamage * levelUpAttackMultiplier); 
        Debug.Log($"<color=cyan>{playerName} attack damage increased to {currentAttackDamage}</color>");
    }

    // calculate the EXP required for the next level up
    private void SetExpLevelUpThreshold()
    {
        expLevelUpThreshold = currentLevel * nextLevelIncrease;
        Debug.Log($"<color=cyan>The {playerName} requires {expLevelUpThreshold} EXP for next level-up</color>");
    }
}
